package posthandler

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/McKael/madon"
	"github.com/turnage/graw/reddit"
)

type Handler struct {
	mastodonClient *madon.Client
}

func NewPostHandler(mc *madon.Client) *Handler {
	var ph Handler
	ph.mastodonClient = mc
	return &ph
}

func (ph *Handler) Post(post *reddit.Post) error {
	if !strings.Contains(post.URL, ".jpg") && !strings.Contains(post.URL, ".png") {
		return nil
	}

	title := post.Title
	url := post.URL

	// download media
	filepath, err := downloadFile(url)
	if err != nil {
		return err
	}

	defer os.Remove(filepath)

	// upload media
	attachment, err := ph.mastodonClient.UploadMedia(filepath, "OP: https://reddit.com"+post.Permalink, "")
	if err != nil {
		return err
	}

	// post it
	_, err = ph.mastodonClient.PostStatus(title, 0, []int64{attachment.ID}, false, "", "")

	log.Println("correctly posted a new image:", post.Permalink)
	return err
}

// got from https://golangcode.com/download-a-file-from-a-url/
// I'm lazy
func downloadFile(url string) (string, error) {
	tmpfile, err := ioutil.TempFile("/tmp", "surrealtoots-")
	if err != nil {
		return "", err
	}

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	_, err = io.Copy(tmpfile, resp.Body)
	if err != nil {
		return "", err
	}

	if err := tmpfile.Close(); err != nil {
		return "", err
	}

	return tmpfile.Name(), nil
}
