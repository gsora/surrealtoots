# surrealtoots: post /r/surrealmemes dankness to Mastodon

Go get-able!

Currently running at `@surrealmemes@botsin.space`.

## How-to

1. install through `go get`
2. create a Mastodon application and populate a `config.toml` file
3. create a script application on Reddit, and populate a `redditConfig.agent` file
4. run!
