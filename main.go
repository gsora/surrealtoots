package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/McKael/madon"
	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
	"gitlab.com/gsora/surrealtoots/config"
	"gitlab.com/gsora/surrealtoots/posthandler"
)

var (
	mastodonClient    *madon.Client
	mastodonUserToken madon.UserToken
	cfg               config.Config
	rBot              reddit.Bot
	cfgPath           string
	redditConfigPath  string
	err               error
)

func main() {
	setCLIParams()

	cfg, err = config.ReadConfig(cfgPath)
	if err != nil {
		log.Fatal(err)
	}

	mastodonUserToken.AccessToken = cfg.AccessToken
	mastodonClient, err = madon.RestoreApp("surrealtoots", "botsin.space", cfg.ClientKey, cfg.ClientSecret, &mastodonUserToken)
	if err != nil {
		log.Fatal(err)
	}

	if rBot, err = reddit.NewBotFromAgentFile(redditConfigPath, 0); err != nil {
		log.Fatal("Failed to create reddit bot", err)
	}

	log.Println("surrealtoots is online!")
	rcfg := graw.Config{Subreddits: []string{"surrealmemes"}}
	handler := posthandler.NewPostHandler(mastodonClient)
	if _, wait, err := graw.Run(handler, rBot, rcfg); err != nil {
		log.Println("Failed to start graw run: ", err)
	} else {
		fmt.Println("graw run failed:", wait())
	}

}

func setCLIParams() {
	flag.StringVar(&cfgPath, "config", "./config.toml", "configuration file path")
	flag.StringVar(&redditConfigPath, "reddit", "./redditConfig.agent", "reddit configuration file path")
	flag.Parse()
}
